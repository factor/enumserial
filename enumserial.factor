

USING: kernel windows.advapi32 windows.types
       alien.data namespaces windows.errors
       tools.continuations math byte-arrays 
       io.encodings.utf16 io.encodings.string
       alien.strings sequences locals ;



IN: enumserial


TUPLE: senum name data ;
SYMBOLS: hserialcomm maxvaluenamelen maxvaluelen ;
SYMBOLS: index valuename dwtype byvalue ;
SYMBOLS: vlist ;



: <senum> ( name data -- senum )
    senum boa ;


: enumserial ( -- )

    ;


! **********************************
! Description - lets generate a list 
!               COM ports
! ***********************************
:: serial-enumerate ( -- vector )
    V{ } clone :> nvector
    0 HKEY <ref> :> hsc
    0 DWORD <ref> :> lpvaluenamelen
    0 DWORD <ref> :> lpdatalen

    HKEY_LOCAL_MACHINE "HARDWARE\\DEVICEMAP\\SERIALCOMM"
    0 KEY_QUERY_VALUE hsc
    RegOpenKeyEx ERROR_SUCCESS =
    [
        hsc HKEY deref
        f f f f f f f
        lpvaluenamelen lpdatalen
        f f
        RegQueryInfoKey ERROR_SUCCESS =
        [
            0 :> dwindex!
            lpvaluenamelen DWORD deref 2 * <byte-array> :> lpvaluename
            lpdatalen  DWORD deref 2 * <byte-array> :> lpdata
            0 DWORD <ref> :> lptype
            [
                break
                hsc HKEY deref
                dwindex
                lpvaluename
                lpvaluename length DWORD <ref>
                f
                lptype
                lpdata
                lpdata length DWORD <ref>
                RegEnumValue ERROR_SUCCESS =
            ]
            [
                break
                lptype DWORD deref REG_SZ =
                [
                    lpdata utf16le alien>string ! decode
                    lpvaluename utf16le alien>string
                    <senum>
                    nvector push dwindex 1 + dwindex!
                ] when
            ] while
        ] when   
    ] when
    nvector
;

: start-enum ( -- )
    HKEY_LOCAL_MACHINE
    "HARDWARE\\DEVICEMAP\\SERIALCOMM"
    0
    KEY_QUERY_VALUE
    0 DWORD <ref> hserialcomm set hserialcomm get
    RegOpenKeyEx
    ERROR_SUCCESS =
    [
        hserialcomm get DWORD deref
        f
        f
        f
        f
        f
        f
        f
        0 DWORD <ref> maxvaluenamelen set maxvaluenamelen get
        0 DWORD <ref> maxvaluelen set maxvaluelen get
        f
        f
        RegQueryInfoKey ERROR_SUCCESS =
        [
            0 index set ! index starts at zero
            V{ } clone vlist set
            [
                break
                hserialcomm get DWORD deref
                index get ! dwIndex
                maxvaluenamelen get DWORD deref 1 + <byte-array>
                valuename set valuename get
                maxvaluenamelen get DWORD deref 1 + DWORD <ref>
                f
                0 DWORD <ref> dwtype set dwtype get
                maxvaluelen get DWORD deref 1 + <byte-array> byvalue set
                byvalue get
                maxvaluelen get
                RegEnumValue ERROR_SUCCESS =
            ]
            [
                break
                dwtype get DWORD deref REG_SZ =
                [
                    byvalue get utf16le alien>string ! decode
                    vlist get push index get 1 + index set
                ] when
            ] while
        ] when

    ] when
    ;
